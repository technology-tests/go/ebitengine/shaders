package shaders

import (
	"embed"
	_ "image/jpeg"
	"log"
	"math"
	"sort"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

//go:embed kage graphics
var embeds embed.FS

func newImageFromFile(path string) *ebiten.Image {
	f, err := embeds.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	i, _, err := ebitenutil.NewImageFromReader(f)
	if err != nil {
		log.Fatal(err)
	}

	return i
}

type shaderFunctions struct {
	draw        func(self *Shader, screen *ebiten.Image)
	setUniforms func(self *Shader, screen *ebiten.Image)
	tick        func(self *Shader)
}

type Shader struct {
	data  map[string]any
	funcs shaderFunctions
}

func (self Shader) Draw(screen *ebiten.Image) {
	self.funcs.draw(&self, screen)
}

func (self Shader) Tick() {
	self.funcs.tick(&self)
}

type ShaderDetails struct {
	Shader     *Shader
	ShaderName string
}

func GetAllShaderDetails() []ShaderDetails {
	shaderDetails := []ShaderDetails{
		ShaderDetails{
			Shader:     NewLightingShader(),
			ShaderName: "Interactive lighting",
		},
		ShaderDetails{
			Shader:     NewMaskingAnimation1Shader(),
			ShaderName: "Masking animation 1",
		},
		ShaderDetails{
			Shader:     NewMaskingAnimation2Shader(),
			ShaderName: "Masking animation 2",
		},
		ShaderDetails{
			Shader:     NewCircleAnimationShader(),
			ShaderName: "Circle animation",
		},
		ShaderDetails{
			Shader:     NewGradientShader(),
			ShaderName: "Gradient (diagonal)",
		},
		ShaderDetails{
			Shader:     NewScaledGradientShader(),
			ShaderName: "Gradient (50%)",
		},
	}

	return shaderDetails
}

func getShaderProgram(shaderPath string) *ebiten.Shader {
	shaderSource, err := embeds.ReadFile(shaderPath)
	if err != nil {
		log.Fatal(err)
	}

	shaderProgram, err := ebiten.NewShader(shaderSource)
	if err != nil {
		log.Fatal(err)
	}

	return shaderProgram
}

func newBaseShader(shaderPath string) *Shader {
	shaderProgram := getShaderProgram(shaderPath)

	shader := &Shader{
		data: make(map[string]any),
		funcs: shaderFunctions{
			draw: func(self *Shader, screen *ebiten.Image) {
				opts := &ebiten.DrawRectShaderOptions{}

				self.funcs.setUniforms(self, screen)

				opts.Uniforms = self.data["uniforms"].(map[string]any)

				screen.DrawRectShader(
					screen.Bounds().Dx(),
					screen.Bounds().Dy(),
					shaderProgram,
					opts,
				)
			},
			setUniforms: func(self *Shader, screen *ebiten.Image) {},
			tick:        func(self *Shader) {},
		},
	}

	shader.data["uniforms"] = make(map[string]any)

	return shader
}

func NewLightingShader() *Shader {
	shaderPath := "kage/lighting.kage"

	shader := newBaseShader(shaderPath)
	shaderProgram := getShaderProgram(shaderPath)

	foregroundImage := newImageFromFile("graphics/suzanne-unlit.jpg")
	normalMap := newImageFromFile("graphics/suzanne-normal.jpg")

	shader.funcs.draw = func(self *Shader, screen *ebiten.Image) {
		opts := &ebiten.DrawRectShaderOptions{}
		opts.Images[0] = foregroundImage
		opts.Images[1] = normalMap

		// Center image on screen
		position := ebiten.GeoM{}
		position.Translate(
			float64(screen.Bounds().Dx()-foregroundImage.Bounds().Dx())/2.00,
			float64(screen.Bounds().Dy()-foregroundImage.Bounds().Dy())/2.00,
		)

		opts.GeoM = position

		self.funcs.setUniforms(self, screen)

		opts.Uniforms = self.data["uniforms"].(map[string]any)

		// opts.Images[*] must be the same size as the area to draw to
		screen.DrawRectShader(
			opts.Images[0].Bounds().Dx(),
			opts.Images[0].Bounds().Dy(),
			shaderProgram,
			opts,
		)
	}

	shader.data["hasPreviousTouch"] = new(bool)
	shader.data["previousTouchX"] = new(int)
	shader.data["previousTouchY"] = new(int)

	shader.funcs.setUniforms = func(self *Shader, screen *ebiten.Image) {
		uniforms := self.data["uniforms"].(map[string]any)

		var touchIDs []ebiten.TouchID
		touchIDs = ebiten.AppendTouchIDs(touchIDs)

		if len(touchIDs) > 0 {
			// Since AppendTouchIDs is not deterministic with the same touches
			// (for example, it might return [1, 2] or [2, 1]),
			// we sort the touches by ID so it does not flicker between frames
			sort.Slice(touchIDs, func(i, j int) bool {
				return touchIDs[i] < touchIDs[j]
			})

			id := touchIDs[0]
			x, y := ebiten.TouchPosition(id)

			uniforms["PointLightPosition"] = []float32{
				float32(x),
				float32(y),
			}

			*(self.data["hasPreviousTouch"].(*bool)) = true
			*(self.data["previousTouchX"].(*int)) = x
			*(self.data["previousTouchY"].(*int)) = y

			return
		} else if *(self.data["hasPreviousTouch"].(*bool)) {
			// If no active touch, use last known touch position
			uniforms["PointLightPosition"] = []float32{
				float32(*(self.data["previousTouchX"].(*int))),
				float32(*(self.data["previousTouchY"].(*int))),
			}

			return
		}

		// If no touches have ever occurred, use cursor position
		// or default to (0, 0)
		cursorX, cursorY := ebiten.CursorPosition()

		uniforms["PointLightPosition"] = []float32{
			float32(cursorX),
			float32(cursorY),
		}
	}

	return shader
}

func NewMaskingAnimation1Shader() *Shader {
	shaderPath := "kage/masking-animation.kage"

	shader := newBaseShader(shaderPath)
	shaderProgram := getShaderProgram(shaderPath)

	shader.data["time"] = new(int)

	foregroundImage := newImageFromFile("graphics/suzanne.jpg")
	maskImage := foregroundImage

	shader.funcs.draw = func(self *Shader, screen *ebiten.Image) {
		opts := &ebiten.DrawRectShaderOptions{}
		opts.Images[0] = foregroundImage
		opts.Images[1] = maskImage

		// Center image on screen
		position := ebiten.GeoM{}
		position.Translate(
			float64(screen.Bounds().Dx()-foregroundImage.Bounds().Dx())/2.00,
			float64(screen.Bounds().Dy()-foregroundImage.Bounds().Dy())/2.00,
		)

		opts.GeoM = position

		self.funcs.setUniforms(self, screen)

		opts.Uniforms = self.data["uniforms"].(map[string]any)

		// opts.Images[*] must be the same size as the area to draw to
		screen.DrawRectShader(
			opts.Images[0].Bounds().Dx(),
			opts.Images[0].Bounds().Dy(),
			shaderProgram,
			opts,
		)
	}

	shader.funcs.setUniforms = func(self *Shader, screen *ebiten.Image) {
		uniforms := self.data["uniforms"].(map[string]any)

		t := self.data["time"].(*int)
		degrees := float64(*t)
		radians := degrees * math.Pi / 180.0

		speed := 1.00

		uniforms["Threshold"] = float32(0.90 * math.Abs(math.Sin(speed*radians)))
	}

	shader.funcs.tick = func(self *Shader) {
		t := self.data["time"].(*int)
		*t += 1
	}

	return shader
}

func NewMaskingAnimation2Shader() *Shader {
	shaderPath := "kage/masking-animation.kage"

	shader := newBaseShader(shaderPath)
	shaderProgram := getShaderProgram(shaderPath)

	shader.data["time"] = new(int)

	foregroundImage := newImageFromFile("graphics/suzanne.jpg")
	noiseImage := newImageFromFile("graphics/noise.jpg")

	shader.funcs.draw = func(self *Shader, screen *ebiten.Image) {
		opts := &ebiten.DrawRectShaderOptions{}
		opts.Images[0] = foregroundImage
		opts.Images[1] = noiseImage

		// Center image on screen
		position := ebiten.GeoM{}
		position.Translate(
			float64(screen.Bounds().Dx()-foregroundImage.Bounds().Dx())/2.00,
			float64(screen.Bounds().Dy()-foregroundImage.Bounds().Dy())/2.00,
		)

		opts.GeoM = position

		self.funcs.setUniforms(self, screen)

		opts.Uniforms = self.data["uniforms"].(map[string]any)

		// opts.Images[*] must be the same size as the area to draw to
		screen.DrawRectShader(
			opts.Images[0].Bounds().Dx(),
			opts.Images[0].Bounds().Dy(),
			shaderProgram,
			opts,
		)
	}

	shader.funcs.setUniforms = func(self *Shader, screen *ebiten.Image) {
		uniforms := self.data["uniforms"].(map[string]any)

		t := self.data["time"].(*int)
		degrees := float64(*t)
		radians := degrees * math.Pi / 180.0

		speed := 1.00

		uniforms["Threshold"] = float32(0.90 * math.Abs(math.Sin(speed*radians)))
	}

	shader.funcs.tick = func(self *Shader) {
		t := self.data["time"].(*int)
		*t += 1
	}

	return shader
}

func NewCircleAnimationShader() *Shader {
	shader := newBaseShader("kage/circle-animation.kage")

	shader.data["time"] = new(int)

	shader.funcs.setUniforms = func(self *Shader, screen *ebiten.Image) {
		uniforms := self.data["uniforms"].(map[string]any)

		uniforms["Center"] = []float32{
			float32(screen.Bounds().Dx()) / 2,
			float32(screen.Bounds().Dy()) / 2,
		}

		t := self.data["time"].(*int)

		degrees := float64(*t)
		radians := degrees * math.Pi / 180.0

		baseRadius := 75.00
		variation := 50.00

		uniforms["Radius"] = float32(baseRadius + variation*math.Sin(radians))
	}

	shader.funcs.tick = func(self *Shader) {
		t := self.data["time"].(*int)
		*t += 1
	}

	return shader
}

func NewGradientShader() *Shader {
	return newBaseShader("kage/gradient.kage")
}

func NewScaledGradientShader() *Shader {
	shader := newBaseShader("kage/gradient-scaled.kage")

	shader.funcs.setUniforms = func(self *Shader, screen *ebiten.Image) {
		uniforms := self.data["uniforms"].(map[string]any)

		uniforms["Height"] = []float32{
			float32(screen.Bounds().Dy()),
		}
	}

	return shader
}
