package menu

import (
	"demo/shaders"

	"image/color"

	"github.com/PerformLine/go-stockutil/colorutil"
	"github.com/ebitenui/ebitenui"
	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
)

func GetUI(listItemSelectCallback func(*shaders.Shader)) *ebitenui.UI {
	rootContainer := newThemedContainer(
		widget.ContainerOpts.BackgroundImage(
			image.NewNineSliceColor(color.RGBA{0.00, 0.00, 0.00, 0.00}),
		),
		widget.ContainerOpts.Layout(
			widget.NewGridLayout(
				widget.GridLayoutOpts.Columns(1),
				widget.GridLayoutOpts.Stretch([]bool{true}, []bool{true}),
				widget.GridLayoutOpts.Padding(
					widget.Insets{
						Top:  20,
						Left: 20,
					},
				),
				widget.GridLayoutOpts.Spacing(10, 10),
			),
		),
	)

	menuContainer := newThemedContainer()
	rootContainer.AddChild(menuContainer)

	shaderList := getShaderList(listItemSelectCallback)
	menuContainer.AddChild(shaderList)

	menuUI := &ebitenui.UI{
		Container: rootContainer,
	}

	return menuUI
}

func hslaToRgba(h, s, l float64, a uint8) color.RGBA {
	r, g, b := colorutil.HslToRgb(h, s, l)

	return color.RGBA{r, g, b, a}
}

func getShaderList(listItemSelectCallback func(*shaders.Shader)) *widget.List {
	shaderDetails := shaders.GetAllShaderDetails()

	entries := make([]interface{}, len(shaderDetails))
	for i, e := range shaderDetails {
		entries[i] = e
	}

	shaderList := newThemedList(
		widget.ListOpts.Entries(entries),
		widget.ListOpts.EntrySelectedHandler(
			func(args *widget.ListEntrySelectedEventArgs) {
				listItemSelectCallback(
					args.Entry.(shaders.ShaderDetails).Shader,
				)
			},
		),
	)

	shaderList.SetSelectedEntry(entries[0])
	listItemSelectCallback(entries[0].(shaders.ShaderDetails).Shader)

	return shaderList
}
