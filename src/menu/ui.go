package menu

import (
	"demo/fonts"
	"demo/shaders"

	"embed"
	"log"

	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

//go:embed ui
var embeds embed.FS

func newImageFromFile(path string) *ebiten.Image {
	f, err := embeds.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	i, _, err := ebitenutil.NewImageFromReader(f)
	if err != nil {
		log.Fatal(err)
	}

	return i
}

func newThemedContainer(opts ...widget.ContainerOpt) *widget.Container {
	nineSliceWidthHeight := 20

	defaultOpts := []widget.ContainerOpt{
		widget.ContainerOpts.BackgroundImage(
			image.NewNineSliceSimple(
				newImageFromFile("ui/nine-slice.png"),
				nineSliceWidthHeight,
				nineSliceWidthHeight,
			),
		),
		widget.ContainerOpts.Layout(
			widget.NewGridLayout(
				widget.GridLayoutOpts.Columns(1),
				widget.GridLayoutOpts.Stretch([]bool{true}, []bool{true}),
				widget.GridLayoutOpts.Padding(
					widget.Insets{
						Top:    12,
						Left:   12,
						Right:  12,
						Bottom: 12,
					},
				),
				widget.GridLayoutOpts.Spacing(10, 10),
			),
		),
	}

	container := widget.NewContainer(
		append(
			defaultOpts,
			opts...,
		)...,
	)

	return container
}

func newThemedList(opts ...widget.ListOpt) *widget.List {
	padding := widget.Insets{
		Left:   15,
		Right:  15,
		Top:    10,
		Bottom: 10,
	}

	entryColor := &widget.ListEntryColor{
		Unselected:         hslaToRgba(200, 1.00, 0.95, 255),
		DisabledUnselected: hslaToRgba(200, 0.50, 0.95, 255),

		Selected:         hslaToRgba(200, 1.00, 0.95, 255),
		DisabledSelected: hslaToRgba(200, 0.50, 0.95, 255),

		SelectedBackground:         hslaToRgba(200, 1.00, 0.30, 255),
		DisabledSelectedBackground: hslaToRgba(200, 0.50, 0.30, 255),

		FocusedBackground:         hslaToRgba(200, 1.00, 0.15, 255),
		SelectedFocusedBackground: hslaToRgba(200, 0.50, 0.15, 255),
	}

	scrollContainerImage := &widget.ScrollContainerImage{
		Idle:     image.NewNineSliceColor(hslaToRgba(200, 1.00, 0.60, 255)),
		Disabled: image.NewNineSliceColor(hslaToRgba(200, 0.50, 0.60, 255)),
		Mask:     image.NewNineSliceColor(hslaToRgba(0.00, 0.00, 0.00, 255)),
	}

	sliderTrackImage := &widget.SliderTrackImage{
		Idle:     image.NewNineSliceColor(hslaToRgba(200, 0.25, 0.75, 255)),
		Hover:    image.NewNineSliceColor(hslaToRgba(200, 0.25, 0.75, 255)),
		Disabled: image.NewNineSliceColor(hslaToRgba(200, 0.15, 0.75, 255)),
	}

	buttonImage := &widget.ButtonImage{
		Idle:     image.NewNineSliceColor(hslaToRgba(200, 0.50, 0.30, 255)),
		Hover:    image.NewNineSliceColor(hslaToRgba(200, 0.50, 0.15, 255)),
		Pressed:  image.NewNineSliceColor(hslaToRgba(200, 0.50, 0.10, 255)),
		Disabled: image.NewNineSliceColor(hslaToRgba(200, 0.25, 0.30, 255)),
	}

	trackPadding := widget.Insets{
		Left:   2,
		Right:  0,
		Top:    0,
		Bottom: 0,
	}

	defaultOpts := []widget.ListOpt{
		widget.ListOpts.EntryLabelFunc(func(e any) string {
			return e.(shaders.ShaderDetails).ShaderName
		}),
		widget.ListOpts.EntryFontFace(fonts.Load(19)),
		widget.ListOpts.EntryTextPadding(padding),
		widget.ListOpts.EntryColor(entryColor),
		widget.ListOpts.ScrollContainerOpts(
			widget.ScrollContainerOpts.Image(scrollContainerImage),
		),
		widget.ListOpts.HideHorizontalSlider(),
		widget.ListOpts.SliderOpts(
			widget.SliderOpts.Images(sliderTrackImage, buttonImage),
			widget.SliderOpts.MinHandleSize(5),
			widget.SliderOpts.TrackPadding(trackPadding),
		),
	}

	list := widget.NewList(
		append(
			defaultOpts,
			opts...,
		)...,
	)

	return list
}
