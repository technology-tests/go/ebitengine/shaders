package main

import (
	"demo/menu"
	"demo/shaders"

	"errors"
	"log"

	"github.com/ebitenui/ebitenui"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

type Game struct {
	isFullscreen bool
	menuUI       *ebitenui.UI
	menuUIImg    *ebiten.Image
	shader       *shaders.Shader
}

// Based on https://github.com/hajimehoshi/ebiten/issues/327#issuecomment-281548037
var NormalTermination = errors.New("terminated")

func (self *Game) Update() error {
	if ebiten.IsKeyPressed(ebiten.KeyControl) {
		if ebiten.IsKeyPressed(ebiten.KeyQ) ||
			ebiten.IsKeyPressed(ebiten.KeyW) {
			return NormalTermination
		}
	}

	if inpututil.IsKeyJustPressed(ebiten.KeyF) ||
		inpututil.IsKeyJustPressed(ebiten.KeyF11) {
		self.isFullscreen = !self.isFullscreen
		ebiten.SetFullscreen(self.isFullscreen)
	}

	if inpututil.IsKeyJustPressed(ebiten.KeyEscape) &&
		self.isFullscreen {
		self.isFullscreen = false
		ebiten.SetFullscreen(self.isFullscreen)
	}

	self.shader.Tick()
	self.menuUI.Update()

	return nil
}

func (self *Game) Draw(screen *ebiten.Image) {
	self.shader.Draw(screen)
	self.menuUI.Draw(self.menuUIImg)

	screen.DrawImage(self.menuUIImg, nil)
}

func (self *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return outsideWidth, outsideHeight
}

func main() {
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowResizingMode(ebiten.WindowResizingModeEnabled)
	ebiten.SetWindowTitle("Demo")
	ebiten.SetTPS(45)

	game := &Game{
		isFullscreen: false,
		menuUI:       nil,
		menuUIImg:    ebiten.NewImage(250, 175),
		shader:       nil,
	}

	game.menuUI = menu.GetUI(
		func(shader *shaders.Shader) {
			game.shader = shader
		},
	)

	if err := ebiten.RunGame(game); err != nil && err != NormalTermination {
		log.Fatal(err)
	}
}
