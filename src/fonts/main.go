package fonts

import (
	"embed"
	"log"

	"github.com/golang/freetype/truetype"

	"golang.org/x/image/font"
	"golang.org/x/image/font/gofont/gomono"
)

//go:embed ttf
var embeds embed.FS

func Load(size float64) font.Face {
	return LoadUbuntu(size)
}

func LoadGo(size float64) font.Face {
	ttfFont, err := truetype.Parse(gomono.TTF)
	if err != nil {
		log.Fatal(err)
	}

	return truetype.NewFace(ttfFont, &truetype.Options{
		Size:    size,
		DPI:     72,
		Hinting: font.HintingFull,
	})
}

func LoadUbuntu(size float64) font.Face {
	ttfSource, err := embeds.ReadFile("ttf/Ubuntu-R.ttf")
	if err != nil {
		log.Fatal(err)
	}

	ttfFont, err := truetype.Parse(ttfSource)
	if err != nil {
		log.Fatal(err)
	}

	return truetype.NewFace(ttfFont, &truetype.Options{
		Size:    size,
		DPI:     72,
		Hinting: font.HintingFull,
	})
}
